package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import fj.data.Either;
import fj.data.Option;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Christoph on 13.10.15.
 * Test methods taken from ac.at.tuwien.dsg.hadl.framework.test.runtime at https://bitbucket.org/christophdorn/hadl (Accessed: 13.10.2015)
 * and modified to comply with HadlLinker interface
 */
public class AcquireReleaseTest extends BaseTest {

    @Test
    public void acquire() {
        HadlLinker l = ps.getLinker();

        String id = "GoogleFile";
        TGoogleFileDescriptor gfd1 = GoogleFactory.createFileDescriptor("gfd1");
        Either<Throwable, THADLarchElement> e = l.acquire(id, gfd1);
        checkAcquire(gfd1, id, e, TOperationalObject.class);

        TGoogleFileDescriptor gfd2 = GoogleFactory.createFileDescriptor("gfd2");
        e = l.acquire(id, gfd2);
        checkAcquire(gfd2, id, e, TOperationalObject.class);

        id = "GoogleDriveUser";
        TGoogleUserDescriptor gud1 = GoogleFactory.createUserDescriptor("gud1");
        e = l.acquire(id, gud1);
        checkAcquire(gud1, id, e, TOperationalComponent.class);

        TGoogleUserDescriptor gud2 = GoogleFactory.createUserDescriptor("gud2");
        e = l.acquire(id, gud2);
        checkAcquire(gud2, id, e, TOperationalComponent.class);
    }

//    @Test
    // not implemented in HADLLinkageConnector or HadlLinker yet
    public void acquireWrongRd() {
        HadlLinker l = ps.getLinker();

        String id = "GoogleFile";
        TGoogleUserDescriptor gfd1 = GoogleFactory.createUserDescriptor("gfd1");
        Either<Throwable, THADLarchElement> e = l.acquire(id, gfd1);
        if (e.isRight()) {
            fail("Returned Operational despite wrong ResourceDescriptor");
        }
    }

    @Test
    public void releaseComponent() {
        String id = "GoogleDriveUser";
        TGoogleUserDescriptor gud1 = GoogleFactory.createUserDescriptor("gud1");
        Either<Throwable, THADLarchElement> e = ps.getLinker().acquire(id, gud1);
        checkAcquire(gud1, id, e, TOperationalComponent.class);

        TOperationalComponent el = (TOperationalComponent) e.right().value();
        Option<Throwable> result = ps.getLinker().release(el);
        checkRelease(el, result);
    }

    @Test
    public void releaseConnector() {
        String id = "GoogleModerator";
        TGoogleUserDescriptor gud1 = GoogleFactory.createUserDescriptor("gud1");
        Either<Throwable, THADLarchElement> e = ps.getLinker().acquire(id, gud1);
        checkAcquire(gud1, id, e, TOperationalConnector.class);

        TOperationalConnector el = (TOperationalConnector) e.right().value();
        Option<Throwable> result = ps.getLinker().release(el);
        checkRelease(el, result);
    }

    @Test
    public void releaseObject() {
        String id = "GoogleFile";
        TGoogleFileDescriptor gfd1 = GoogleFactory.createFileDescriptor("gfd1");
        Either<Throwable, THADLarchElement> e = ps.getLinker().acquire(id, gfd1);
        checkAcquire(gfd1, id, e, TOperationalObject.class);

        TOperationalObject el = (TOperationalObject) e.right().value();
        Option<Throwable> result = ps.getLinker().release(el);
        checkRelease(el, result);
    }

    private <T extends THADLarchElement> void checkAcquire(TResourceDescriptor originalRd, String elementId, Either<Throwable, THADLarchElement> result, Class<T> expectedResult) {
        if (result.isLeft()) {
            fail("Error aquiring element: " + result.left().value().getMessage());
        } else {
            THADLarchElement originalStatic = ps.getModelTypesUtil().getById(elementId);
            assertTrue("Incorrect type", expectedResult.isInstance(result.right().value()));
            T el = expectedResult.cast(result.right().value());

            TTypeRef typeRef = null;
            List<TResourceDescriptor> rds = null;
            if (el instanceof TOperationalComponent) {
                TOperationalComponent cel = (TOperationalComponent) el;
                typeRef = cel.getInstanceOf();
                rds = cel.getResourceDescriptor();
                assertTrue("Operational not acquired (not in correct state)", TOperationalState.DESCRIBED_EXISTING.equals(cel.getState()));
                assertTrue("RuntimeModel does not contain element", ps.getRuntimeModel().getComponent().contains(cel));
            } else if (el instanceof TOperationalConnector) {
                TOperationalConnector cel = (TOperationalConnector) el;
                typeRef = cel.getInstanceOf();
                rds = cel.getResourceDescriptor();
                assertTrue("Operational not acquired (not in correct state)", TOperationalState.DESCRIBED_EXISTING.equals(cel.getState()));
                assertTrue("RuntimeModel does not contain element", ps.getRuntimeModel().getConnector().contains(cel));
            } else if (el instanceof TOperationalObject) {
                TOperationalObject cel = (TOperationalObject) el;
                typeRef = cel.getInstanceOf();
                rds = cel.getResourceDescriptor();
                assertTrue("Operational not acquired (not in correct state)", TOperationalState.DESCRIBED_EXISTING.equals(cel.getState()));
                assertTrue("RuntimeModel does not contain element", ps.getRuntimeModel().getObject().contains(cel));
            } else {
                // can never happen
                return;
            }

            THADLarchElement staticEl = ps.getModelTypesUtil().getByTypeRef(typeRef);
            assertEquals("Acquire returned a wrong operational", originalStatic.getId(), staticEl.getId());
            assertTrue("Operational does not contain ResourceDscriptor", rds.contains(originalRd));
        }
    }

    private <T extends THADLarchElement> void checkRelease(T el, Option<Throwable> result) {
        if (result.isSome()) {
            fail("Error releasing element: " + result.some());
        } else {
            TOperationalState state = null;
            if (el instanceof TOperationalComponent) {
                state = ((TOperationalComponent) el).getState();
                assertTrue("Element not released", TOperationalState.PRESCRIBED_NONEXISTING.equals(state) || TOperationalState.DESCRIBED_NONEXISTING.equals(state));
            } else if (el instanceof TOperationalConnector) {
                state = ((TOperationalConnector) el).getState();
                assertTrue("Element not released", TOperationalState.PRESCRIBED_NONEXISTING.equals(state) || TOperationalState.DESCRIBED_NONEXISTING.equals(state));
            } else if (el instanceof TOperationalObject) {
                state = ((TOperationalObject) el).getState();
                System.out.println(state);
                assertTrue("Element not released", TOperationalState.PRESCRIBED_NONEXISTING.equals(state) || TOperationalState.DESCRIBED_NONEXISTING.equals(state));
            } else {
                // does not happen
            }
        }
    }
}
