package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import fj.data.Either;
import fj.data.Option;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Christoph on 14.10.15.
 * Test methods taken from ac.at.tuwien.dsg.hadl.framework.test.runtime at https://bitbucket.org/christophdorn/hadl (Accessed: 13.10.2015)
 * and modified to comply with HadlLinker interface
 */
public class ReferenceDereferenceTest extends BaseTest {

    @Test
    public void referenceCollaborators() {
        Either<Throwable, TOperationalCollabRef> result = collabRef("isModerator");
        if (result.isLeft()) {
            fail(result.left().value().getMessage());
        }
        TOperationalCollabRef cref = result.right().value();
        assertTrue("Operational in incorrect state", TOperationalState.DESCRIBED_EXISTING.equals(cref.getState()));
        assertTrue("Operational not in RuntimeModel", ps.getRuntimeModel().getCollabRef().contains(cref));
    }

    private Either<Throwable, TOperationalCollabRef> collabRef(String refId) {
        HadlLinker l = ps.getLinker();
        // acquire collaborators
        String id = "GoogleDriveUser";
        TGoogleUserDescriptor gud1 = GoogleFactory.createUserDescriptor("gud1");
        Either<Throwable, THADLarchElement> e = l.acquire(id, gud1);
        // no checks, handled by other test
        TOperationalComponent user = (TOperationalComponent) e.right().value();

        id = "GoogleModerator";
        TGoogleUserDescriptor gud2 = GoogleFactory.createUserDescriptor("gud2");
        e = l.acquire(id, gud2);
        // no checks, handled by other test
        TOperationalConnector moderator = (TOperationalConnector) e.right().value();
        return l.reference(user, moderator, refId);
    }

    @Test
    public void referenceObjects() {
        Either<Throwable, TOperationalObjectRef> result = objectRef("subfolder");
        if (result.isLeft()) {
            fail(result.left().value().getMessage());
        }
        TOperationalObjectRef ref = result.right().value();
        assertTrue("Operational in incorrect state", TOperationalState.DESCRIBED_EXISTING.equals(ref.getState()));
        assertTrue("Operational not in RuntimeModel", ps.getRuntimeModel().getObjectRef().contains(ref));
    }

    private Either<Throwable, TOperationalObjectRef> objectRef(String refId) {
        HadlLinker l = ps.getLinker();
        // acquire objects
        String id = "GoogleFile";
        TGoogleFileDescriptor gfd1 = GoogleFactory.createFileDescriptor("gfd1");
        Either<Throwable, THADLarchElement> e = l.acquire(id, gfd1);
        // no checks, handled by other test
        TOperationalObject f1 = (TOperationalObject) e.right().value();

        TGoogleFileDescriptor gfd2 = GoogleFactory.createFileDescriptor("gfd2");
        e = l.acquire(id, gfd2);
        // no checks, handled by other test
        TOperationalObject f2 = (TOperationalObject) e.right().value();
        return l.reference(f1, f2, refId);
    }

    @Test
    public void dereferenceCollaborators() {
        // no checks, handled by other test
        TOperationalCollabRef ref = collabRef("isModerator").right().value();

        Option<Throwable> res = ps.getLinker().dereference(ref);
        if (res.isSome()) {
            fail(res.some().getMessage());
        }
        assertTrue("Operational not in correct state", TOperationalState.DESCRIBED_NONEXISTING.equals(ref.getState()));
    }

    @Test
    public void dereferenceObjects() {
        // no checks, handled by other test
        TOperationalObjectRef ref = objectRef("subfolder").right().value();

        Option<Throwable> res = ps.getLinker().dereference(ref);
        if (res.isSome()) {
            fail(res.some().getMessage());
        }
        assertTrue("Operational in incorrect state", TOperationalState.DESCRIBED_NONEXISTING.equals(ref.getState()));
    }
}
