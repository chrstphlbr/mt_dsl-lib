package net.laaber.mt.dsl.lib.hadl.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.*;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import net.laaber.mt.dsl.lib.hadl.GoogleFactory;
import rx.subjects.BehaviorSubject;

import java.util.Set;

/**
 * Created by Christoph on 19.10.15.
 */
public class FileFolderSensor extends AbstractAsyncSensor {

    private final ModelTypesUtil mtu;
    private final int config;

    FileFolderSensor(ModelTypesUtil mtu, int config) {
        this.mtu = mtu;
        this.config = config;
    }

    @Override
    public void asyncAcquire(final TActivityScope forScope, final TOperationalComponent sensingOrigin, final BehaviorSubject<SensorEvent> subject) {
        subject.onCompleted();
    }

    @Override
    public void asyncAcquire(final TActivityScope forScope, final TOperationalConnector sensingOrigin, final BehaviorSubject<SensorEvent> subject) {
        subject.onCompleted();
    }

    @Override
    public void asyncAcquire(final TActivityScope forScope, final TOperationalObject sensingOrigin, final BehaviorSubject<SensorEvent> subject) {
        subject.onCompleted();
    }

    @Override
    public void asyncLoadOnce(final BehaviorSubject<LoadEvent> subject, final SensingScope scope) {
        TCollabObject o = (TCollabObject) mtu.getByTypeRef(super.oc.getInstanceOf());

        Set<TCollabLink> links = scope.getLinks();
        for (TCollabLink l : links) {
            if (!o.getId().equals(mtu.getObjectForAction(l.getObjActionEndpoint()).getId())) {
                continue;
            }

            switch (l.getId()) {
                case "google-drive-owning-file":
                case "google-drive-writing-file":
                case "google-drive-reading-file":
                case "google-drive-owning-folder":
                case "google-drive-writing-folder":
                case "google-drive-reading-folder":
                    emitUsers(subject, scope, l);
                    break;
            }
        }

        Set<TObjectRef> refs = scope.getObjectRefs();
        for (TObjectRef r : refs) {
            if (!o.getId().equals(r.getObjFrom().getId())) {
                continue;
            }

            switch (o.getId()) {
                case "GoogleFile":
                    switch (r.getId()) {
                        // to file
                        case "templateFile":
                            emitFiles(subject, scope, r);
                            break;
                    }
                    break;
                case "GoogleFolder":
                    switch (r.getId()) {
                        // to folder
                        case "subfolder":
                            emitFolders(subject, scope, r);
                            break;
                        // to file
                        case "fileInfolder":
                            emitFiles(subject, scope, r);
                            break;
                    }
                    break;
            }
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        subject.onCompleted();
    }

    private void emitUsers(BehaviorSubject<LoadEvent> s, SensingScope scope, TCollabLink l) {
        OperativeCollaboratorEvent e = new OperativeCollaboratorEvent((TCollaborator) mtu.getById("GoogleDriveUser"), super.oc, scope);
        e.addLoadedViaLinkType(l);
        for (int i = 1; i <= 2; i++) {
            TResourceDescriptor r = GoogleFactory.createUserDescriptor("user" + i);
            e.addDescriptor(r);
        }
        s.onNext(e);
    }

    private void emitFiles(BehaviorSubject<LoadEvent> s, SensingScope scope, TObjectRef r) {
        OperativeObjectEvent e = new OperativeObjectEvent((TCollabObject) mtu.getById("GoogleFile"), super.oc, scope);
        e.addLoadedViaRef(r);
        for (int i = 1; i <= 2; i++) {
            TResourceDescriptor rd = GoogleFactory.createFileDescriptor("file" + i);
            e.addDescriptor(rd);
        }
        s.onNext(e);
    }

    private void emitFolders(BehaviorSubject<LoadEvent> s, SensingScope scope, TObjectRef r) {
        OperativeObjectEvent e = new OperativeObjectEvent((TCollabObject) mtu.getById("GoogleFolder"), super.oc, scope);
        e.addLoadedViaRef(r);
        for (int i = 1; i <= 2; i++) {
            TResourceDescriptor rd = GoogleFactory.createFileDescriptor("folder" + i);
            e.addDescriptor(rd);
        }
        s.onNext(e);
    }
}
