package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.*;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.FactoryResolvingException;
import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import com.google.inject.*;
import fj.data.Either;
import fj.data.Option;
import net.laaber.mt.dsl.lib.hadl.sensor.TestingGoogleUserMockSensorFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Christoph on 07.10.15.
 * Test methods taken from ac.at.tuwien.dsg.hadl.framework.test.runtime at https://bitbucket.org/christophdorn/hadl (Accessed: 13.10.2015)
 * and modified to comply with HadlLinker interface
 */
public class BaseTest {

    private final static String MODEL_FILE = "./src/test/resources/input/GoogleDrive.xml";
    private final static String OUTPUT_PATH_PREFIX = "src/test/resources/output/gdrive/";

    protected ProcessScope ps;

    @Rule
    public TestName testName = new TestName();

    @Before
    public void _setUp() throws Throwable {
        final HadlModelInteractor mi = new HadlModelInteractorImpl();
        Either<Throwable, HADLmodel> loadResult = mi.load(MODEL_FILE);
        assertTrue(loadResult.isRight());
        final HADLmodel model = loadResult.right().value();

        final Injector baseInjector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
            }

            @Provides
            @Singleton
            Executable2OperationalTransformer getTransformer() {
                return new Executable2OperationalTransformer();
            }

            @Provides
            @Singleton
            ModelTypesUtil getTypesUtil() {
                ModelTypesUtil mtu = new ModelTypesUtil();
                mtu.init(model);
                return mtu;
            }
        });

        final Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(HADLLinkageConnector.class).in(Singleton.class);
                bind(HADLruntimeMonitor.class).in(Singleton.class);
                bind(HadlLinker.class).to(HadlLinkerImpl.class).in(Singleton.class);
                bind(HadlMonitor.class).to(HadlMonitorImpl.class).in(Singleton.class);
                bind(ProcessScope.class).in(Singleton.class);
                bind(SensorFactory.class).to(TestingGoogleUserMockSensorFactory.class).in(Singleton.class);
            }

            @Provides
            @Singleton
            HadlModelInteractor getModelInteractor() {
                return mi;
            }

            @Provides
            @Singleton
            Executable2OperationalTransformer getTransformer() {
                return baseInjector.getInstance(Executable2OperationalTransformer.class);
            }

            @Provides
            @Singleton
            RuntimeRegistry getRegistry() {
                return new RuntimeRegistry();
            }

            @Provides
            @Singleton
            ModelTypesUtil getTypesUtil() {
                return baseInjector.getInstance(ModelTypesUtil.class);
            }

            @Provides
            @Singleton
            SurrogateFactoryResolver getFactoryResolver() {
                SurrogateFactoryResolver sfr = new SurrogateFactoryResolver();
                try {
                    sfr.resolveFactoriesFrom(model);
                } catch (FactoryResolvingException e) {
                    e.printStackTrace();
                }
                return sfr;
            }

            @Provides
            @Singleton
            HADLruntimeModel getRuntimeModel() {
                Neo4JbackedRuntimeModel hrm = new Neo4JbackedRuntimeModel();
                baseInjector.injectMembers(hrm);
                // setup Neo4J DB
                GraphDatabaseService graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
                hrm.init(graphDb, model);
                return hrm;
            }
        });
        ps = injector.getInstance(ProcessScope.class);

        initLinker();
    }

    @Before
    public void printBeginDelimiter() {
        System.out.println("---------- " + testName.getMethodName() + " BEGIN");
    }

    @After
    public void printAfterDelimiter() {
        System.out.println("---------- " + testName.getMethodName() + " END");
    }

    @After
    public void _tearDown() {
        storeRt(testName.getMethodName() + ".xml");
        ps.getLinkageConnector().shutdown();
        ps = null;
    }

    private void initLinker() {
        Option<Throwable> setUpError = ps.getLinker().setUp(null);
        assertTrue(setUpError.isNone());
    }

    private void storeRt(String fileName) {
        List<String> e = new ArrayList<>();
        e.add("at.ac.tuwien.dsg.hadl.schema.extension.google");
        Option<Throwable> r = ps.getModelInteractor().storeRuntime(ps.getModel(), ps.getRuntimeModel(), e, OUTPUT_PATH_PREFIX, fileName);
        if (r.isSome()) {
            r.some().printStackTrace();
        }
    }
}
