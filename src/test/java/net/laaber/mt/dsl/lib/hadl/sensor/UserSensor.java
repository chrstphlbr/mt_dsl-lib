package net.laaber.mt.dsl.lib.hadl.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.*;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import net.laaber.mt.dsl.lib.hadl.GoogleFactory;
import rx.subjects.BehaviorSubject;

import java.util.Set;

/**
 * Created by Christoph on 15.10.15.
 */
public class UserSensor extends AbstractAsyncSensor {

    private final ModelTypesUtil mtu;
    private final int config;

    UserSensor(ModelTypesUtil mtu, int config) {
        this.mtu = mtu;
        this.config = config;
    }

    @Override
    public void asyncAcquire(final TActivityScope forScope, final TOperationalComponent sensingOrigin, final BehaviorSubject<SensorEvent> subject) {
        subject.onCompleted();
    }

    @Override
    public void asyncAcquire(final TActivityScope forScope, final TOperationalConnector sensingOrigin, final BehaviorSubject<SensorEvent> subject) {
        subject.onCompleted();
    }

    @Override
    public void asyncAcquire(final TActivityScope forScope, final TOperationalObject sensingOrigin, final BehaviorSubject<SensorEvent> subject) {
        subject.onCompleted();
    }

    @Override
    public void asyncLoadOnce(final BehaviorSubject<LoadEvent> subject, final SensingScope scope) {
        String id = "";
        if (oc1 != null) {
            THumanComponent c = (THumanComponent) mtu.getByTypeRef(super.oc1.getInstanceOf());
            id = c.getId();
        } else if (oc2 != null) {
            TCollabConnector c = (TCollabConnector) mtu.getByTypeRef(super.oc2.getInstanceOf());
            id = c.getId();
        } else {
            // should never be the case
            throw new IllegalStateException("oc1 and oc2 are null");
        }

        // only consider links
        Set<TCollabLink> links = scope.getLinks();
        for (TCollabLink l : links) {
            if (!id.equals(mtu.getCollaboratorForAction(l.getCollabActionEndpoint()).getId())) {
                continue;
            }
            // is a link connected to this operational (oc1)
            switch (l.getId()) {
                case "google-drive-owning-file":
                case "google-drive-writing-file":
                case "google-drive-reading-file":
                    emitFiles(subject, scope, l);
                    break;
                case "google-drive-owning-folder":
                case "google-drive-writing-folder":
                case "google-drive-reading-folder":
                    emitFolders(subject, scope, l);
                    break;
            }
        }

        Set<TCollabRef> refs = scope.getCollabRefs();
        for (TCollabRef r : refs) {
            if (!r.getCompconnFrom().getId().equals(id)) {
                continue;
            }
            switch (r.getId()) {
                case "isModerator":
                    if (config == 1) {
                        OperativeCollaboratorEvent e = new OperativeCollaboratorEvent((TCollabConnector) mtu.getById("GoogleModerator"), super.oc1, scope);
                        TGoogleUserDescriptor d = GoogleFactory.createUserDescriptor("mod" + 1);
                        e.addDescriptor(d);
                        e.addLoadedViaRef(r);
                        subject.onNext(e);
                    }
                    break;
                case "isModeratorRev":
                    emitUsers(subject, scope, r);
                    break;
            }
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        subject.onCompleted();
    }

    private void emitUsers(BehaviorSubject<LoadEvent> s, SensingScope sc, TCollabRef ref) {
        if (config != 1) {
            return;
        }
        OperativeCollaboratorEvent e = new OperativeCollaboratorEvent((TCollaborator) mtu.getById("GoogleDriveUser"), super.oc2, sc);
        e.addLoadedViaRef(ref);
        for (int i = 1; i <= 2; i++) {
            TGoogleUserDescriptor d = GoogleFactory.createUserDescriptor("user" + i);
            e.addDescriptor(d);
        }
        s.onNext(e);
    }

    private void emitFiles(BehaviorSubject<LoadEvent> s, SensingScope scope, TCollabLink l) {
        OperativeObjectEvent e = new OperativeObjectEvent((TCollabObject) mtu.getById("GoogleFile"), super.oc1, scope);
        e.addLoadedViaLinkType(l);
        for (int i = 1; i <= 2; i++) {
            TGoogleFileDescriptor d = GoogleFactory.createFileDescriptor("file" + i);
            e.addDescriptor(d);
        }
        s.onNext(e);
    }

    private void emitFolders(BehaviorSubject<LoadEvent> s, SensingScope scope, TCollabLink l) {
        OperativeObjectEvent e = new OperativeObjectEvent((TCollabObject) mtu.getById("GoogleFolder"), super.oc1, scope);
        TGoogleFileDescriptor d = GoogleFactory.createFileDescriptor("folder" + 1);
        e.addLoadedViaLinkType(l);
        e.addDescriptor(d);
        s.onNext(e);
    }
}
