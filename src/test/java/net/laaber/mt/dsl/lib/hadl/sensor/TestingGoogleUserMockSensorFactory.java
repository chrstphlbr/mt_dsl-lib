package net.laaber.mt.dsl.lib.hadl.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabObjectSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollaboratorSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.InsufficientModelInformationException;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import com.google.inject.Inject;

public class TestingGoogleUserMockSensorFactory implements SensorFactory {

    private final ModelTypesUtil mtu;
    private int config;

    @Inject
	public TestingGoogleUserMockSensorFactory(ModelTypesUtil mtu) {
        this.mtu = mtu;
    }

    public void setConfig(int config) {
        this.config = config;
    }
	
	@Override
	public ICollabSensor getInstance(THADLarchElement type) throws InsufficientModelInformationException {
		if (type instanceof TCollaborator) {
            return getInstance((TCollaborator) type);
        } else if (type instanceof TCollabObject) {
            return getInstance((TCollabObject) type);
        }
        // should never happen
        org.junit.Assert.fail("type of type " + type.getClass().getSimpleName());
		return null;
	}

	@Override
	public ICollaboratorSensor getInstance(TCollaborator type) throws InsufficientModelInformationException {
        return new UserSensor(mtu, config);
	}

	@Override
	public ICollabObjectSensor getInstance(TCollabObject type) throws InsufficientModelInformationException {
		return new FileFolderSensor(mtu, config);
	}

}
