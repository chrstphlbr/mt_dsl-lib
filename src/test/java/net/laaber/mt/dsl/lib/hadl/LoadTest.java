package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import fj.data.Either;
import net.laaber.mt.dsl.lib.hadl.sensor.TestingGoogleUserMockSensorFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Christoph on 15.10.15.
 * Test methods taken from ac.at.tuwien.dsg.hadl.framework.test.runtime at https://bitbucket.org/christophdorn/hadl (Accessed: 13.10.2015)
 * and modified to comply with HadlMonitor interface
 */
public class LoadTest extends BaseTest {

    private TestingGoogleUserMockSensorFactory fac;

    @Before
    public void setUp() {
        fac = (TestingGoogleUserMockSensorFactory) ps.getSensorFactory();
    }

    @Test
    public void incorrectWhatNotLoadableWrongElementId() {
        String linkId = "google-drive-owning-file";
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleDriveUser", linkId);
        incorrectVias(what, null, "Can not load 'GoogleDriveUser' from 'GoogleDriveUser' via 'google-drive-owning-file'");
    }

    @Test
    public void incorrectWhatNotLoadableWrongLink() {
        String linkId = "subfolder";
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", linkId);
        incorrectVias(what, null, "Can not load 'GoogleFile' from 'GoogleDriveUser' via 'subfolder'");
    }

    @Test
    public void incorrectWhatHadlElementId() {
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleDrive", "google-drive-owning-file");
        incorrectVias(what, null, "No hADL element (from) for ID 'GoogleDrive'");
    }

    @Test
    public void incorrectWhatViaId() {
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleDriveUser", "AAAgoogle-drive-owning-file");
        incorrectVias(what, null, "No hADL element (via) for ID 'AAAgoogle-drive-owning-file'");
    }

    private void incorrectVias(Map.Entry<String, String> what, List<Map.Entry<String, String>> vias, String expectedError) {
        fac.setConfig(0);
        // acquire user
        String id = "GoogleDriveUser";
        TGoogleUserDescriptor gud1 = GoogleFactory.createUserDescriptor("gud1");
        TOperationalComponent e = (TOperationalComponent) ps.getLinker().acquire(id, gud1).right().value();
        // no checks, done by other test

        // load users via link
        Either<Throwable, List<THADLarchElement>> result = ps.getMonitor().load(what, e, vias);
        if (result.isRight()) {
            fail("failure expected");
        }
        String msg = result.left().value().getMessage();
        assertTrue("wrong exception (" + msg + ")", expectedError.equals(msg));
    }

    @Test
    public void incorrectViasIncorrectHadlElementId() {
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "google-drive-owning-file");
        List<Map.Entry<String, String>> vias = new ArrayList<>();
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>("GoogleDrive", "google-drive-owning-file"));
        incorrectVias(what, vias, "No hADL element (from) for ID 'GoogleDrive'");
    }

    @Test
    public void incorrectViasIncorrectViaId() {
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "google-drive-owning-file");
        List<Map.Entry<String, String>> vias = new ArrayList<>();
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "AAAgoogle-drive-owning-file"));
        incorrectVias(what, vias, "No hADL element (via) for ID 'AAAgoogle-drive-owning-file'");
    }

    @Test
    public void incorrectViasNotLoadable1() {
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "google-drive-owning-file");
        List<Map.Entry<String, String>> vias = new ArrayList<>();
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "google-drive-owning-file"));
        incorrectVias(what, vias, "Can not load 'GoogleFile' from 'GoogleFile' via 'google-drive-owning-file'");
    }

    @Test
    public void incorrectViasNotLoadable2() {
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleDriveUser", "google-drive-reading-file");
        List<Map.Entry<String, String>> vias = new ArrayList<>();
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "google-drive-owning-file"));
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>("GoogleDriveUser", "google-drive-reading-file"));
        incorrectVias(what, vias, "Can not load 'GoogleDriveUser' from 'GoogleDriveUser' via 'google-drive-reading-file'");
    }

    @Test
    public void loadCompSingleStep() {
        fac.setConfig(0);
        // acquire user
        String id = "GoogleDriveUser";
        TGoogleUserDescriptor gud1 = GoogleFactory.createUserDescriptor("gud1");
        TOperationalComponent e = (TOperationalComponent) ps.getLinker().acquire(id, gud1).right().value();
        // no checks, done by other test

        // load users via link
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "google-drive-owning-file");
        Either<Throwable, List<THADLarchElement>> result = ps.getMonitor().load(what, e, null);
        if (result.isLeft()) {
            fail(result.left().value().getMessage());
        }
        List<THADLarchElement> elements = result.right().value();
        assertTrue("incorrect number of elements. expected 2, is " + elements.size(), elements.size() == 2);
        for (THADLarchElement el : elements) {
            assertTrue("incorrect element type " + el.getClass().getSimpleName(), el instanceof TOperationalObject);
        }
    }

    @Test
    public void loadCompMultiStep() {
        fac.setConfig(0);
        // acquire user
        String id = "GoogleDriveUser";
        TGoogleUserDescriptor gud1 = GoogleFactory.createUserDescriptor("gud1");
        TOperationalComponent e = (TOperationalComponent) ps.getLinker().acquire(id, gud1).right().value();
        // no checks, done by other test

        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "templateFile");
        List<Map.Entry<String, String>> vias = new ArrayList<>();
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "google-drive-owning-file"));
        Either<Throwable, List<THADLarchElement>> result = ps.getMonitor().load(what, e, vias);
        if (result.isLeft()) {
            fail(result.left().value().getMessage());
        }
        List<THADLarchElement> elements = result.right().value();
        assertTrue("incorrect number of elements. expected 4, is " + elements.size(), elements.size() == 4);
        for (THADLarchElement el : elements) {
            assertTrue("incorrect element type " + el.getClass().getSimpleName(), el instanceof TOperationalObject);
        }
    }

    @Test
    public void loadConnSingleStep() {
        fac.setConfig(1);
        // acquire user
        String id = "GoogleModerator";
        TGoogleUserDescriptor gud1 = GoogleFactory.createUserDescriptor("gud1");
        TOperationalConnector e = (TOperationalConnector) ps.getLinker().acquire(id, gud1).right().value();
        // no checks, done by other test

        // load users via collab ref
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleDriveUser", "isModeratorRev");
        Either<Throwable, List<THADLarchElement>> result = ps.getMonitor().load(what, e, null);
        if (result.isLeft()) {
            fail(result.left().value().getMessage());
        }
        List<THADLarchElement> elements = result.right().value();
        assertTrue("incorrect number of elements. expected 2, is " + elements.size(), elements.size() == 2);
        for (THADLarchElement el : elements) {
            assertTrue("incorrect element type " + el.getClass().getSimpleName(), el instanceof TOperationalComponent);
        }
    }

    @Test
    public void loadConnMultiStep() {
        fac.setConfig(1);
        // acquire user
        String id = "GoogleModerator";
        TGoogleUserDescriptor gud1 = GoogleFactory.createUserDescriptor("gud1");
        TOperationalConnector e = (TOperationalConnector) ps.getLinker().acquire(id, gud1).right().value();
        // no checks, done by other test

        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "google-drive-reading-file");
        List<Map.Entry<String, String>> vias = new ArrayList<>();
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>("GoogleDriveUser", "isModeratorRev"));
        Either<Throwable, List<THADLarchElement>> result = ps.getMonitor().load(what, e, vias);
        if (result.isLeft()) {
            fail(result.left().value().getMessage());
        }
        List<THADLarchElement> elements = result.right().value();
        assertTrue("incorrect number of elements. expected 2, is " + elements.size(), elements.size() == 4);
        for (THADLarchElement el : elements) {
            assertTrue("incorrect element type " + el.getClass().getSimpleName(), el instanceof TOperationalObject);
        }
    }

    @Test
    public void loadObjSingleStep() {
        fac.setConfig(0);
        // acquire user
        String id = "GoogleFolder";
        TGoogleFileDescriptor d = GoogleFactory.createFileDescriptor("folder");
        TOperationalObject e = (TOperationalObject) ps.getLinker().acquire(id, d).right().value();
        // no checks, done by other test

        // load file vie objectref
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "fileInfolder");
        List<Map.Entry<String, String>> vias = new ArrayList<>();
        Either<Throwable, List<THADLarchElement>> result = ps.getMonitor().load(what, e, vias);
        if (result.isLeft()) {
            fail(result.left().value().getMessage());
        }
        List<THADLarchElement> elements = result.right().value();
        assertTrue("incorrect number of elements. expected 2, is " + elements.size(), elements.size() == 2);
        for (THADLarchElement el : elements) {
            assertTrue("incorrect element type " + el.getClass().getSimpleName(), el instanceof TOperationalObject);
        }
    }

    @Test
    public void loadObjMultiStep() {
        fac.setConfig(1);
        // acquire user
        String id = "GoogleFolder";
        TGoogleFileDescriptor d = GoogleFactory.createFileDescriptor("folder");
        TOperationalObject e = (TOperationalObject) ps.getLinker().acquire(id, d).right().value();
        // no checks, done by other test

        // load file vie objectref
        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>("GoogleModerator", "isModerator");
        List<Map.Entry<String, String>> vias = new ArrayList<>();
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>("GoogleFile", "fileInfolder"));
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>("GoogleDriveUser", "google-drive-writing-file"));
        Either<Throwable, List<THADLarchElement>> result = ps.getMonitor().load(what, e, vias);
        if (result.isLeft()) {
            fail(result.left().value().getMessage());
        }
        List<THADLarchElement> elements = result.right().value();
        assertTrue("incorrect number of elements. expected 2, is " + elements.size(), elements.size() == 4);
        for (THADLarchElement el : elements) {
            assertTrue("incorrect element type " + el.getClass().getSimpleName(), el instanceof TOperationalConnector);
        }
    }
}
