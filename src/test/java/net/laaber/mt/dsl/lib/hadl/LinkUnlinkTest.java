package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalState;
import fj.data.Either;
import fj.data.Option;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Christoph on 14.10.15.
 * Test methods taken from ac.at.tuwien.dsg.hadl.framework.test.runtime at https://bitbucket.org/christophdorn/hadl (Accessed: 13.10.2015)
 * and modified to comply with HadlLinker interface
 */
public class LinkUnlinkTest extends BaseTest {

    private static final String link = "google-drive-reading-file";

    private Either<Throwable, TOperationalCollabLink> link(String link) {
        HadlLinker l = ps.getLinker();

        String id = "GoogleFile";
        TGoogleFileDescriptor gfd = GoogleFactory.createFileDescriptor("gfd");
        Either<Throwable, THADLarchElement> e = l.acquire(id, gfd);
        // no checks, handled by other test
        TOperationalObject o = (TOperationalObject) e.right().value();

        id = "GoogleDriveUser";
        TGoogleUserDescriptor gud = GoogleFactory.createUserDescriptor("gud");
        e = l.acquire(id, gud);
        // no checks, handled by other test
        TOperationalComponent c = (TOperationalComponent) e.right().value();

        return l.link(c, o, link);
    }

    @Test
    public void link() {
        Either<Throwable, TOperationalCollabLink> l = link(link);
        if (l.isLeft()) {
            fail(l.left().value().getMessage());
        }
        TOperationalCollabLink link = l.right().value();
        assertTrue("Operational not in RuntimeModel", ps.getRuntimeModel().getLink().contains(link));
        assertTrue("Operational not in correct state", TOperationalState.DESCRIBED_EXISTING.equals(link.getState()));
    }

    @Test
    public void unlink() {
        Either<Throwable, TOperationalCollabLink> l = link(link);
        TOperationalCollabLink link = l.right().value();

        Option<Throwable> res = ps.getLinker().unlink(link);
        if (res.isSome()) {
            fail(res.some().getMessage());
        }
        assertTrue("Operational not in correct state", TOperationalState.DESCRIBED_NONEXISTING.equals(link.getState()));
    }

}
