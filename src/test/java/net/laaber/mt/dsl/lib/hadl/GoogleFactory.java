package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;

/**
 * Created by Christoph on 15.10.15.
 */
public class GoogleFactory {

    private GoogleFactory() {}

    public static TGoogleFileDescriptor createFileDescriptor(String id) {
        TGoogleFileDescriptor gfd = new TGoogleFileDescriptor();
        gfd.setName("Mock file" + id);
        gfd.setId(id);
        return gfd;
    }

    public static TGoogleUserDescriptor createUserDescriptor(String id) {
        TGoogleUserDescriptor gud = new TGoogleUserDescriptor();
        gud.setEmail(id + "@google.com");
        gud.setId(id);
        return gud;
    }

}
