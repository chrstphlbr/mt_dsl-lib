package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.runtime.TRuntimeStructure;
import fj.data.Either;
import fj.data.Option;
import java.util.List;

public interface HadlModelInteractor {
	Either<Throwable, HADLmodel> load(String path);
	Option<Throwable> storeRuntime(HADLmodel model, TRuntimeStructure structure, List<String> extensions, String path, String fileName);
	Option<Throwable> storeExecutable(HADLmodel model, String path, String fileName);
	Option<TActivityScope> scope(HADLmodel model, String id);
}