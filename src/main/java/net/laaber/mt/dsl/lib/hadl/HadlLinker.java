package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import fj.data.Either;
import fj.data.Option;

import java.util.List;

public interface HadlLinker {
	Option<Throwable> setUp(String scopeId);
	Either<Throwable, THADLarchElement> acquire(String archElem, TResourceDescriptor resourceDescriptor);
    Option<Throwable> release(TOperationalComponent c);
    Option<Throwable> release(TOperationalConnector c);
    Option<Throwable> release(TOperationalObject o);
    // precondition: from and to are either TOperationalComponent or TOperationalConnector
    Either<Throwable, TOperationalCollabRef> reference(TCollaborator from, TCollaborator to, String refId);
    Option<Throwable> dereference(TOperationalCollabRef ref);
    Either<Throwable, TOperationalObjectRef> reference(TOperationalObject from, TOperationalObject to, String refId);
    Option<Throwable> dereference(TOperationalObjectRef ref);
    // precondition: collaborator is either TOperationalComponent or TOperationalConnector
    Either<Throwable, TOperationalCollabLink> link(TCollaborator collaborator, TOperationalObject object, String linkId);
    Option<Throwable> unlink(TOperationalCollabLink link);
    Option<Throwable> startScope();
    Option<Throwable> stopScope();
	Option<Throwable> tearDown();
}
