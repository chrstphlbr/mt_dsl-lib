package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLLinkageConnector;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.EhADLstatus;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.StatusEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateEvent;
import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import fj.data.Either;
import fj.data.Option;
import rx.Observable;
import rx.Subscriber;

import javax.inject.Inject;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;

public class HadlLinkerImpl implements HadlLinker {
	
	private static final String defaultScopeId = "default";
	
	private HadlModelInteractor modelInteractor;
	private HADLLinkageConnector lc;
	
	private HADLmodel model;
	private ModelTypesUtil mtu;
	private TActivityScope scope;
	private boolean initialised = false;
	
	private final List<Entry<Entry<TCollaborator, TCollaborator>, TCollabRef>> collabRef = new LinkedList<>();
	private final List<Entry<Entry<TOperationalObject, TOperationalObject>, TObjectRef>> objRef = new LinkedList<>();
		
	@Inject
	public HadlLinkerImpl(HadlModelInteractor modelInteractor, HADLLinkageConnector lc, HADLmodel model, ModelTypesUtil mtu) {
		this.modelInteractor = modelInteractor;
		this.lc = lc;
		this.model = model;
		this.mtu = mtu;
	}
	
	public Option<Throwable> setUp(String scopeId) {
		if (initialised) {
			return Option.none();
		}
		
		if (model == null) {
			return Option.some(new NullPointerException("model is null"));
		}

        scopeId = (scopeId == null || "".equals(scopeId)) ? defaultScopeId : scopeId;
		Option<TActivityScope> scopeResult = modelInteractor.scope(model, scopeId);
		if (scopeResult.isNone()) {
			// error handling
			return Option.some(new IllegalArgumentException("No scope with id: " + scopeId));
		}
		scope = scopeResult.some();
		
		// initialise linkage connector
		Option<Throwable> initResult = initLinkageConnector();
		if (initResult.isNone()) {
			initialised = true;
		}
		
		return initResult;
	}
	
	private Option<Throwable> initLinkageConnector() {
		Observable<StatusEvent> o = lc.init(scope);
		final CountDownLatch l = new CountDownLatch(1);
		final List<Throwable> error = new ArrayList<>();
		o.subscribe(new Subscriber<StatusEvent>() {
			@Override
            public void onCompleted() {
				l.countDown();
			}

            @Override
            public void  onError(Throwable t) {
				System.out.println(t.getMessage());
				error.add(t);
				l.countDown();
			}

            @Override
            public void  onNext(StatusEvent t) {
				System.out.println(t.getStatus());
				if (EhADLstatus.INIT_COMPLETED.equals(t.getStatus())) {
					onCompleted();
				}
			}
		});
		try {
			l.await();
			if (error.isEmpty()) {
				return Option.none();
			} else {
				return Option.some(error.get(0));
			}
		} catch (Throwable e) {
			return Option.some(e);
		}
	}
	
	// acquire returns an operational element that can be acquired (either: TOperationalConnector, TOperationalComponent, TOperationalObject)
	public Either<Throwable, THADLarchElement> acquire(String archElem, TResourceDescriptor resourceDescriptor) {
		THADLarchElement el = mtu.getById(archElem);
		if (el == null) {
			return Either.left(new IllegalArgumentException("No element with id '" + archElem + "'"));
		}
		// check if archElement/net.laaber.mt.dsl.lib.hadl.resourceDescriptor are compatible
		if (!checkElementRd(el, resourceDescriptor)) {
			return Either.left(new IllegalArgumentException("archElem (" + el.getId() + ") not compatible with ResourceDescriptor (" + resourceDescriptor.getId() + ")"));
		}
		List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<>();
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(el, resourceDescriptor));
		Observable<SurrogateEvent> observable = lc.acquireResourcesAsElements(mapping);
		return operationalOrException(THADLarchElement.class, observable);
	}

    @Override
    public Option<Throwable> release(TOperationalComponent c) {
        return waitForSuccess(lc.release(c));
    }

    @Override
    public Option<Throwable> release(TOperationalConnector c) {
        return waitForSuccess(lc.release(c));
    }

    @Override
    public Option<Throwable> release(TOperationalObject o) {
        return waitForSuccess(lc.release(o));
    }

    @Override
	public Either<Throwable, TOperationalCollabRef> reference(TCollaborator from, TCollaborator to, String refId) {
        THADLarchElement el = mtu.getById(refId);
        if (el == null || !(el instanceof TCollabRef)) {
            return Either.left(new IllegalArgumentException("No element exists for id '" + refId + "' or element not of Type " + TCollabRef.class.getSimpleName()));
        }
		TCollabRef ref = (TCollabRef) el;

        List<Entry<TCollaborator, TCollaborator>> fromTo = new ArrayList<>();
        fromTo.add(new AbstractMap.SimpleEntry<>(from, to));

        Observable<SurrogateEvent> o = lc.buildRelations(fromTo, ref, false);
        return operationalOrException(TOperationalCollabRef.class, o);
	}

	@Override
	public Option<Throwable> dereference(TOperationalCollabRef ref) {
        List<Entry<TCollaborator, TCollaborator>> fromTo = new ArrayList<>();
        fromTo.add(new AbstractMap.SimpleEntry<>(ref.getCompconnFrom(), ref.getCompconnTo()));
		TCollabRef staticRef = (TCollabRef) mtu.getByTypeRef(ref.getInstanceOf());
		return waitForSuccess(lc.buildRelations(fromTo, staticRef, true));
	}

	@Override
	public Either<Throwable, TOperationalObjectRef> reference(TOperationalObject from, TOperationalObject to, String refId) {
        THADLarchElement el = mtu.getById(refId);
        if (el == null || !(el instanceof TObjectRef)) {
            return Either.left(new IllegalArgumentException("No element exists for id '" + refId + "' or element not of Type " + TObjectRef.class.getSimpleName()));
        }
        TObjectRef ref = (TObjectRef) el;

        List<Entry<TOperationalObject, TOperationalObject>> fromTo = new ArrayList<>();
        fromTo.add(new AbstractMap.SimpleEntry<>(from, to));

        Observable<SurrogateEvent> o = lc.buildRelations(fromTo, ref, false);
        return operationalOrException(TOperationalObjectRef.class, o);
	}

	@Override
	public Option<Throwable> dereference(TOperationalObjectRef ref) {
        List<Entry<TOperationalObject, TOperationalObject>> fromTo = new ArrayList<>();
        fromTo.add(new AbstractMap.SimpleEntry<>((TOperationalObject) ref.getObjFrom(), (TOperationalObject) ref.getObjTo()));
        TObjectRef staticRef = (TObjectRef) mtu.getByTypeRef(ref.getInstanceOf());
		return waitForSuccess(lc.buildRelations(fromTo, staticRef, true));
	}

	private boolean checkElementRd(THADLarchElement el, TResourceDescriptor rd) {
		//TODO: do actual checking here
		return true;
	}

	@Override
	public Either<Throwable, TOperationalCollabLink> link(TCollaborator collaborator, TOperationalObject object, String linkId) {
        THADLarchElement archElem = mtu.getById(linkId);
        if (archElem == null || !(archElem instanceof TCollabLink)) {
            return Either.left(new IllegalArgumentException("No link found for id: " + linkId));
        }
        TCollabLink link = (TCollabLink) archElem;

        // check if collaborator has action that is compatible with link
        TCollaborator execCollaborator;
        if (collaborator instanceof  TOperationalComponent) {
            execCollaborator = (THumanComponent) mtu.getByTypeRef(((TOperationalComponent) collaborator).getInstanceOf());
        } else if (collaborator instanceof TOperationalConnector) {
            execCollaborator = (TCollabConnector) mtu.getByTypeRef(((TOperationalConnector) collaborator).getInstanceOf());
        } else {
            // should not be the case according to precondition
            return Either.left(new IllegalArgumentException("collaborator is not of type TOperationalComponent or TOperationalConnector"));
        }

        TCollabObject execObject = (TCollabObject) mtu.getByTypeRef(object.getInstanceOf());

        if (!mtu.isLinkable(execCollaborator, execObject, link)) {
            return Either.left(new IllegalArgumentException("collaborator and object can not be linked"));
        }

        //TODO: check if already linked

        // do linking
        Observable<SurrogateEvent> o = null;
        if (collaborator instanceof TOperationalComponent) {
            o = lc.wire((TOperationalComponent) collaborator, object, link);
        } else if (collaborator instanceof TOperationalConnector) {
            o = lc.wire((TOperationalComponent) collaborator, object, link);
        } else {
            return Either.left(new IllegalArgumentException("collaborator is neither TOperationalComponent nor TOperationalConnector"));
        }

        return operationalOrException(TOperationalCollabLink.class, o);
	}

	@Override
	public Option<Throwable> unlink(TOperationalCollabLink link) {
        Observable<SurrogateEvent> o = lc.unwire(link);
        return waitForSuccess(o);
	}
	
	private void removeReferences() {
		List<Observable<SurrogateEvent>> os = new LinkedList<>();
		for (Entry<Entry<TCollaborator, TCollaborator>, TCollabRef> r : collabRef) {
			List<Entry<TCollaborator, TCollaborator>> l = new ArrayList<>();
			l.add(r.getKey());
			os.add(lc.buildRelations(l, r.getValue(), true));
		}
		
		for (Entry<Entry<TOperationalObject, TOperationalObject>, TObjectRef> r : objRef) {
			List<Entry<TOperationalObject, TOperationalObject>> l = new ArrayList<>();
			l.add(r.getKey());
			os.add(lc.buildRelations(l, r.getValue(), true));
		}
		
		Observable<SurrogateEvent> o = Observable.merge(os);
		o.subscribe(new Subscriber<SurrogateEvent>() {
            @Override
            public void onCompleted() {
				System.out.println("Successfully removed references");
			}

            @Override
            public void onError(Throwable arg0) {
				System.out.println("Error removing reference: ");
				arg0.printStackTrace();
			}

            @Override
            public void onNext(SurrogateEvent arg0) {
                // nothing to do here
			}
		});
	}

	@Override
	public Option<Throwable> startScope() {
		return waitForSuccess(lc.startScope());
	}

	@Override
	public Option<Throwable> stopScope() {
		return waitForSuccess(lc.stopScope());
	}

	private void unlink() {
		Observable<SurrogateEvent> o = lc.unwireAllActions();
        Option<Throwable> failure = waitForSuccess(o);
        if (failure.isSome()) {
            System.out.println("Error unlinking actions: " + failure.some().getMessage());
            failure.some().printStackTrace();
        } else {
            System.out.println("Successfully unlinked Actions");
        }
	}

    private Option<Throwable> waitForSuccess(Observable<SurrogateEvent> o) {
        CountDownLatch l = new CountDownLatch(1);

        List<Option<Throwable>> ret = new ArrayList<>();
        o.subscribe(new Subscriber<SurrogateEvent>() {
            @Override
            public void onCompleted() {
                ret.add(Option.none());
				l.countDown();
            }

            @Override
            public void onError(Throwable throwable) {
                ret.add(Option.some(throwable));
				l.countDown();
            }

            @Override
            public void onNext(SurrogateEvent surrogateEvent) {
                Option<SurrogateStateChangeFailure> failure = failure(surrogateEvent);
                if (failure.isSome()) {
                    onError(failure.some());
                }
            }
        });

        try {
            l.await();
        } catch (InterruptedException e) {
            return Option.some(e);
        }
        return ret.get(0);
    }
	
	private <O extends THADLarchElement> Either<Throwable, O> operationalOrException(Class<O> retClass, Observable<SurrogateEvent> o) {
		CountDownLatch l = new CountDownLatch(1);
		// dirty placeholder trick
		List<Either<Throwable, O>> ph = new ArrayList<>();
		o.subscribe(new Subscriber<SurrogateEvent>() {
			@Override
			public void onCompleted() {
				l.countDown();
			}

			@Override
			public void onError(Throwable arg0) {
				ph.add(Either.left(arg0));
				l.countDown();
			}

            @Override
			public void onNext(SurrogateEvent arg0) {
				Option<SurrogateStateChangeFailure> failure = failure(arg0);
                if (failure.isSome()) {
                    onError(failure.some());
                } else {
                    ph.add(Either.right((O) arg0.getOperationalElement()));
                }
			}
		});
		// wait until operational is acquired
        try {
            l.await();
        } catch (InterruptedException e) {
            return Either.left(e);
        }
        // dirty placeholder trick
        return ph.get(0);
	}

    private Option<SurrogateStateChangeFailure> failure(SurrogateEvent e) {
        switch (e.getStatus()) {
            case ACQUIRING_FAILED:
            case REWIRING_FAILED:
            case RELATING_FAILED:
            case STARTING_FAILED:
            case STOPPING_FAILED:
            case RELEASING_FAILED: return Option.some(new SurrogateStateChangeFailure(e));
            default: return Option.none();
        }
    }
	
	public Option<Throwable> tearDown() {
		unlink();
		removeReferences();
		lc.releaseAllResources();
		return Option.none();
	}
	
}
