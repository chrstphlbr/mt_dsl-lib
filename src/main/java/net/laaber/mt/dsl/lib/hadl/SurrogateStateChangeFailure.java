package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateEvent;

/**
 * Created by Christoph on 22.09.15.
 */
public class SurrogateStateChangeFailure extends Exception {

    private final SurrogateEvent e;

    public SurrogateStateChangeFailure(SurrogateEvent e) {
        super(e.getStatus().toString(), e.getOptionalEx());
        this.e = e;
    }

    public SurrogateEvent surrogateEvent() {
        return e;
    }
}
