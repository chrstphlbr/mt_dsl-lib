package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;

public class HadlElements {
	
	private static ModelTypesUtil mtu = null;

	private HadlElements() {}
	
	public static void setModelTypesUtil(ModelTypesUtil mtu) {
        HadlElements.mtu = mtu;
	}
	
	public static ModelTypesUtil modelTypesUtil() {
		return HadlElements.mtu;
	} 
}