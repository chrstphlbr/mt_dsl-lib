package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import fj.data.Either;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface HadlMonitor {
    /**
     * load loads hADL elements (what) starting from a TOperationalComponent startingFrom via fromVias.
     *
     * @param what what to load. key of the entry is the id of hADL element and value is the id of the hADL reference/link
     * @param startingFrom the starting point from which to load elements
     * @param fromVias intermediate steps between startingFrom and what in order. entry keys and values are of same type as the ones from what
     * @return either a Throwable indicating an error or the corresponding elements defined by what.key
     */
	Either<Throwable, List<THADLarchElement>> load(Map.Entry<String, String> what, TOperationalComponent startingFrom, List<Map.Entry<String, String>> fromVias);

    /**
     * load loads hADL elements (what) starting from a TOperationalConnector startingFrom via fromVias.
     *
     * @param what what to load. key of the entry is the id of hADL element and value is the id of the hADL reference/link
     * @param startingFrom the starting point from which to load elements
     * @param fromVias intermediate steps between startingFrom and what in order. entry keys and values are of same type as the ones from what
     * @return either a Throwable indicating an error or the corresponding elements defined by what.key
     */
	Either<Throwable, List<THADLarchElement>> load(Map.Entry<String, String> what, TOperationalConnector startingFrom, List<Map.Entry<String, String>> fromVias);

    /**
     * load loads hADL elements (what) starting from a TOperationalObject startingFrom via fromVias.
     *
     * @param what what to load. key of the entry is the id of hADL element and value is the id of the hADL reference/link
     * @param startingFrom the starting point from which to load elements
     * @param fromVias intermediate steps between startingFrom and what in order. entry keys and values are of same type as the ones from what
     * @return either a Throwable indicating an error or the corresponding elements defined by what.key
     */
	Either<Throwable, List<THADLarchElement>> load(Map.Entry<String, String> what, TOperationalObject startingFrom, List<Map.Entry<String, String>> fromVias);

    default Either<Throwable, Set<THADLarchElement>> loadWithoutDuplicates(Map.Entry<String, String> what, TOperationalComponent startingFrom, List<Map.Entry<String, String>> fromVias) {
        Either<Throwable, List<THADLarchElement>> l = load(what, startingFrom, fromVias);
        if (l.isLeft()) {
            return Either.left(l.left().value());
        }
        return Either.right(new HashSet<>(l.right().value()));
    }

    default Either<Throwable, Set<THADLarchElement>> loadWithoutDuplicates(Map.Entry<String, String> what, TOperationalConnector startingFrom, List<Map.Entry<String, String>> fromVias) {
        Either<Throwable, List<THADLarchElement>> l = load(what, startingFrom, fromVias);
        if (l.isLeft()) {
            return Either.left(l.left().value());
        }
        return Either.right(new HashSet<>(l.right().value()));
    }

    default Either<Throwable, Set<THADLarchElement>> loadWithoutDuplicates(Map.Entry<String, String> what, TOperationalObject startingFrom, List<Map.Entry<String, String>> fromVias) {
        Either<Throwable, List<THADLarchElement>> l = load(what, startingFrom, fromVias);
        if (l.isLeft()) {
            return Either.left(l.left().value());
        }
        return Either.right(new HashSet<>(l.right().value()));
    }
}