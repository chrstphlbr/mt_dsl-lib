package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.*;
import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;

import javax.inject.Inject;

/**
 * Created by Christoph on 03.09.15.
 */
public class ProcessScope {

    private HADLmodel model;
    private HADLruntimeModel runtimeModel;
    private ModelTypesUtil modelTypesUtil;
    private HADLLinkageConnector linkageConnector;
    private HADLruntimeMonitor runtimeMonitor;
    private SurrogateFactoryResolver surrogateFactoryResolver;
    private SensorFactory sensorFactory;
    private Executable2OperationalTransformer transformer;
    private RuntimeRegistry runtimeRegistry;

    private HadlModelInteractor hmi;
    private HadlLinker linker;
    private HadlMonitor monitor;

    public ProcessScope() {}

    public HADLmodel getModel() {
        return model;
    }

    @Inject
    public void setModel(HADLmodel model) {
        this.model = model;
    }

    public HADLruntimeModel getRuntimeModel() {
        return runtimeModel;
    }

    @Inject
    public void setRuntimeModel(HADLruntimeModel runtimeModel) {
        this.runtimeModel = runtimeModel;
    }

    public ModelTypesUtil getModelTypesUtil() {
        return modelTypesUtil;
    }

    @Inject
    public void setModelTypesUtil(ModelTypesUtil modelTypesUtil) {
        this.modelTypesUtil = modelTypesUtil;
    }

    public HADLLinkageConnector getLinkageConnector() {
        return linkageConnector;
    }

    @Inject
    public void setLinkageConnector(HADLLinkageConnector linkageConnector) {
        this.linkageConnector = linkageConnector;
    }

    public HADLruntimeMonitor getRuntimeMonitor() {
        return runtimeMonitor;
    }

    @Inject
    public void setRuntimeMonitor(HADLruntimeMonitor runtimeMonitor) {
        this.runtimeMonitor = runtimeMonitor;
    }

    public SurrogateFactoryResolver getSurrogateFactoryResolver() {
        return surrogateFactoryResolver;
    }

    @Inject
    public void setSurrogateFactoryResolver(SurrogateFactoryResolver surrogateFactory) {
        this.surrogateFactoryResolver = surrogateFactory;
    }

    public SensorFactory getSensorFactory() {
        return sensorFactory;
    }

    @Inject
    public void setSensorFactory(SensorFactory sensorFactory) {
        this.sensorFactory = sensorFactory;
    }

    public Executable2OperationalTransformer getTransformer() {
        return transformer;
    }

    @Inject
    public void setTransformer(Executable2OperationalTransformer transformer) {
        this.transformer = transformer;
    }

    public RuntimeRegistry getRuntimeRegistry() {
        return runtimeRegistry;
    }

    @Inject
    public void setRuntimeRegistry(RuntimeRegistry runtimeRegistry) {
        this.runtimeRegistry = runtimeRegistry;
    }

    public HadlModelInteractor getModelInteractor() {
        return hmi;
    }

    @Inject
    public void setModelInteractor(HadlModelInteractor hmi) {
        this.hmi = hmi;
    }

    public HadlLinker getLinker() {
        return linker;
    }

    @Inject
    public void setLinker(HadlLinker linker) {
        this.linker = linker;
    }

    public HadlMonitor getMonitor() {
        return monitor;
    }

    @Inject
    public void setMonitor(HadlMonitor monitor) {
        this.monitor = monitor;
    }
}
