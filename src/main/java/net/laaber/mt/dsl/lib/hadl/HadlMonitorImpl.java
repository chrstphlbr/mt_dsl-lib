package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.LoadEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensingScope;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeMonitor;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.*;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import fj.data.Either;
import fj.data.Option;
import org.apache.commons.lang3.tuple.MutablePair;
import rx.Observable;
import rx.Observer;

import javax.inject.Inject;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class HadlMonitorImpl implements HadlMonitor {
	
	private final HADLruntimeMonitor monitor;
	private final HadlLinker linker;
	private final ModelTypesUtil mtu;
	private final HADLmodel model;
	
	@Inject
	public HadlMonitorImpl(HADLruntimeMonitor monitor, HadlLinker linker, HADLmodel model, ModelTypesUtil mtu) {
		this.monitor = monitor;
		this.linker = linker;
		this.mtu = mtu;
		this.model = model;
	}

	public Either<Throwable, List<THADLarchElement>> load(Map.Entry<String, String> what, TOperationalComponent startingFrom, List<Map.Entry<String, String>> fromVias) {
        return internalLoad(what, startingFrom, fromVias);
	}

    public Either<Throwable, List<THADLarchElement>> load(Map.Entry<String, String> what, TOperationalConnector startingFrom, List<Map.Entry<String, String>> fromVias) {
        return internalLoad(what, startingFrom, fromVias);
	}
	
	public Either<Throwable, List<THADLarchElement>> load(Map.Entry<String, String> what, TOperationalObject startingFrom, List<Map.Entry<String, String>> fromVias) {
		return internalLoad(what, startingFrom, fromVias);
	}

    // precondition: startingFrom is either TOperationalObject, TOperationalConnector or TOperationalComponent
    private Either<Throwable, List<THADLarchElement>> internalLoad(Map.Entry<String, String> what, THADLarchElement startingFrom, List<Map.Entry<String, String>> fromVias) {
        Either<Throwable, List<Map.Entry<THADLarchElement, THADLarchElement>>> fromsElements = fromViasToArchElements(startingFrom, what, fromVias);
        if (fromsElements.isLeft()) {
            return Either.left(fromsElements.left().value());
        }
        List<Map.Entry<THADLarchElement, THADLarchElement>> froms = fromsElements.right().value();
        return internalLoad(startingFrom, froms);
    }

    private Either<Throwable, List<THADLarchElement>> internalLoad(THADLarchElement startingFrom, List<Map.Entry<THADLarchElement, THADLarchElement>> froms) {
        Either<Throwable, SensingScope> sScope = null;
        Observable<LoadEvent> o = null;
        if (startingFrom instanceof TOperationalComponent) {
            TOperationalComponent sf = (TOperationalComponent) startingFrom;
            sScope = sensingScope(mtu.getByTypeRef(sf.getInstanceOf()), froms);
            if (sScope.isLeft()) {
                return Either.left(sScope.left().value());
            }
            o = monitor.loadFrom(sf, sScope.right().value());
        } else if (startingFrom instanceof  TOperationalConnector) {
            TOperationalConnector sf = (TOperationalConnector) startingFrom;
            sScope = sensingScope(mtu.getByTypeRef(sf.getInstanceOf()), froms);
            if (sScope.isLeft()) {
                return Either.left(sScope.left().value());
            }
            o = monitor.loadFrom(sf, sScope.right().value());
        } else if (startingFrom instanceof TOperationalObject) {
            TOperationalObject sf = (TOperationalObject) startingFrom;
            sScope = sensingScope(mtu.getByTypeRef(sf.getInstanceOf()), froms);
            if (sScope.isLeft()) {
                return Either.left(sScope.left().value());
            }
            o = monitor.loadFrom(sf, sScope.right().value());
        } else {
            // according to precondition should never be the case
            throw new IllegalArgumentException("startingFrom of type: " + startingFrom.getClass());
        }

        Either<Throwable, List<THADLarchElement>> ret = internalLoad(o);
        // return on error
        if (ret.isLeft()) {
            return ret;
        }

        // recursive search if still elements left
        if (froms.size() > 1) {
            List<THADLarchElement> es = new ArrayList<>();
            froms = froms.subList(1, froms.size());
            for (THADLarchElement e : ret.right().value()) {
                ret = internalLoad(e, froms);
                if (ret.isLeft()) {
                    return ret;
                } else {
                    es.addAll(ret.right().value());
                }
            }
            return Either.right(es);
        } else {
            return ret;
        }
    }
	
	private Either<Throwable, List<THADLarchElement>> internalLoad(Observable<LoadEvent> o) {
		CountDownLatch l = new CountDownLatch(1);
		MutablePair<Throwable, List<THADLarchElement>> p = new MutablePair<Throwable, List<THADLarchElement>>();
		LoadObserver observer = new LoadObserver(l, p);
		o.subscribe(observer);
        try {
            l.await();
        } catch (InterruptedException e) {
            return Either.left(e);
        }
        if (p.left != null) {
			return Either.left(p.left);
		}
		return Either.right(p.right);
	}
	
	private Either<Throwable, List<Map.Entry<THADLarchElement, THADLarchElement>>> fromViasToArchElements(THADLarchElement startingFrom, Map.Entry<String, String> what, List<Map.Entry<String, String>> fromVias) {
		List<Map.Entry<THADLarchElement, THADLarchElement>> f = new ArrayList<>();
        THADLarchElement staticStartingFrom = null;
        if (startingFrom instanceof TOperationalComponent) {
            staticStartingFrom = mtu.getByTypeRef(((TOperationalComponent) startingFrom).getInstanceOf());
        } else if (startingFrom instanceof TOperationalConnector) {
            staticStartingFrom = mtu.getByTypeRef(((TOperationalConnector) startingFrom).getInstanceOf());
        } else if (startingFrom instanceof TOperationalObject) {
            staticStartingFrom = mtu.getByTypeRef(((TOperationalObject) startingFrom).getInstanceOf());
        } else {
            // should never be the case
            return Either.left(new IllegalStateException("startingFrom is invalid " + startingFrom.getClass().getSimpleName()));
        }

		if (fromVias != null) {
			for (Map.Entry<String, String> fromVia : fromVias) {
				THADLarchElement fe = mtu.getById(fromVia.getKey());
				if (fe == null) {
					return Either.left(new IllegalArgumentException("No hADL element (from) for ID '" + fromVia.getKey() + "'"));
				}
				THADLarchElement ve = mtu.getById(fromVia.getValue());
                if (ve == null) {
                    return Either.left(new IllegalArgumentException("No hADL element (via) for ID '" + fromVia.getValue() + "'"));
                }
                // check if provided data is valid
                Option<Throwable> scopeable = scopeable(staticStartingFrom, fe, ve, f);
                if (scopeable.isSome()) {
                    return Either.left(scopeable.some());
                } else {
                    staticStartingFrom = fe;
                }
			}
		}
		
		THADLarchElement w = mtu.getById(what.getKey());
		if (w == null) {
			return Either.left(new IllegalArgumentException("No hADL element (from) for ID '" + what.getKey() + "'"));
		}
        THADLarchElement wv = mtu.getById(what.getValue());
        if (wv == null) {
            return Either.left(new IllegalArgumentException("No hADL element (via) for ID '" + what.getValue() + "'"));
        }
        Option<Throwable> scopeable = scopeable(staticStartingFrom, w, wv, f);
        if (scopeable.isSome()) {
            return Either.left(scopeable.some());
        }
		
		return Either.right(f);
	}

    private Option<Throwable> scopeable(THADLarchElement startingFrom, THADLarchElement to, THADLarchElement via, List<Map.Entry<THADLarchElement, THADLarchElement>> f) {
        if (mtu.isReferenceableLinkable(startingFrom, to, via)) {
            f.add(new AbstractMap.SimpleEntry<>(to, via));
            return Option.none();
        } else {
            String id = "";
            if (startingFrom instanceof TOperationalComponent) {
                id = mtu.getByTypeRef(((TOperationalComponent) startingFrom).getInstanceOf()).getId();
            } else if (startingFrom instanceof  TOperationalConnector) {
                id = mtu.getByTypeRef(((TOperationalConnector) startingFrom).getInstanceOf()).getId();
            } else if (startingFrom instanceof TOperationalObject) {
                id = mtu.getByTypeRef(((TOperationalObject) startingFrom).getInstanceOf()).getId();
            }
            return Option.some(new IllegalArgumentException("Can not load '" + to.getId() + "' from '" + startingFrom.getId() + "' via '" + via.getId() + "'"));
        }
    }
	
	// startingFrom must be an executable model element (no operational)
	private Either<Throwable, SensingScope> sensingScope(THADLarchElement startingFrom, List<Map.Entry<THADLarchElement, THADLarchElement>> fromVias) {
		SensingScope s = new SensingScope();
        // only compute scope for next fromVia
        if (fromVias.size() == 0) {
            return Either.left(new IllegalArgumentException("fromVias has no elements"));
        }
        Map.Entry<THADLarchElement, THADLarchElement> to = fromVias.get(0);
        Option<Throwable> r = addToSensingScope(s, startingFrom, to.getKey(), to.getValue());
        if (r.isSome()) {
            return Either.left(r.some());
        }
		return Either.right(s);
	}
	
	private Option<Throwable> addToSensingScope(SensingScope s, THADLarchElement from, THADLarchElement to, THADLarchElement via) {
		if ((from instanceof TCollaborator) && (to instanceof TCollaborator)) {
			// collab refs
			TCollaborator f = (TCollaborator) from;
            TCollaborator t = (TCollaborator) to;
            if (via instanceof TCollabRef) {
                s.getCollabRefs().add((TCollabRef) via);
            } else {
                return Option.some(new IllegalArgumentException("Tried to add SensingScope. Not a TCollabRef: " + via.getId()));
            }
		} else if ((from instanceof TCollabObject) && (to instanceof TCollabObject)) {
			// object refs
            TCollabObject f = (TCollabObject) from;
            TCollabObject t = (TCollabObject) to;
            if (via instanceof TObjectRef) {
                s.getObjectRefs().add((TObjectRef) via);
            } else {
                return Option.some(new IllegalArgumentException("Tried to add SensingScope. Not a TObjectRef: " + via.getId()));
            }
		} else {
			// from and to are different; either TCollabObject or TCollaborator
            if (via instanceof TCollabLink) {
                s.getLinks().add((TCollabLink) via);
            } else {
                return Option.some(new IllegalArgumentException("Tried to add SensingScope. Not a TCollabLink: " + via.getId()));
            }
		}
		return Option.none();
	}
	
	// assumes that id of object and collaborator is prefix of id of actions in link
	// assumes that from and to are ids of executable element ids
	private List<TCollabLink> links(String from, String to) {
		List<TCollabLink> links = new ArrayList<>();
		String o;
		String c;
		for (TStructure struct : model.getHADLstructure()) {
			for (TCollabLink link : struct.getLink()) {
				o = link.getObjActionEndpoint().getId();
				c = link.getCollabActionEndpoint().getId();
				if (o.startsWith(from) && c.startsWith(to) || c.startsWith(from) && o.startsWith(to)) {
					// there exists a link wrt assumption
					links.add(link);
				}
			}
		}
		return links;
	}
	
	private static class LoadObserver implements Observer<LoadEvent> {
		
		private final CountDownLatch l;
		private final  MutablePair<Throwable, List<THADLarchElement>> ret;
		
		private LoadObserver(CountDownLatch l, MutablePair<Throwable, List<THADLarchElement>> ret) {
			this.l = l;
			this.ret = ret;
			ret.right = new ArrayList<THADLarchElement>();
		}

        @Override
        public synchronized void onCompleted() {
            l.countDown();
		}

        @Override
        public synchronized void onError(Throwable arg0) {
            ret.left = arg0;
			l.countDown();
		}

        @Override
        public synchronized  void onNext(LoadEvent arg0) {
            ret.right.addAll(arg0.getDescriptorsAndOperationalElements().values());
		}
	}
}
