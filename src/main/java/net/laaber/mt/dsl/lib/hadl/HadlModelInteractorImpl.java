package net.laaber.mt.dsl.lib.hadl;

import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TStructure;
import at.ac.tuwien.dsg.hadl.schema.runtime.ObjectFactory;
import at.ac.tuwien.dsg.hadl.schema.runtime.TRuntimeStructure;
import fj.data.Either;
import fj.data.Option;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

public class HadlModelInteractorImpl implements HadlModelInteractor {
	
	private static final String os = System.getProperty("os.name").toLowerCase();

	public HadlModelInteractorImpl() {}
	
	public Either<Throwable, HADLmodel> load(String path) {
		try {
			JAXBContext jc = JAXBContext.newInstance(at.ac.tuwien.dsg.hadl.schema.core.ObjectFactory.class, at.ac.tuwien.dsg.hadl.schema.executable.ObjectFactory.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
			File xml = new File(osSpecificPath(path));
			return Either.right((HADLmodel)unmarshaller.unmarshal(xml));
//		} catch (JAXBException e) {
		} catch (Throwable t) {
			return Either.left(t);
		}
	}

    public Option<Throwable> storeRuntime(HADLmodel model, TRuntimeStructure structure, List<String> extensions, String path, String fileName) {
		try {
            ObjectFactory of = new ObjectFactory();

            StringBuilder sb = new StringBuilder();
            sb.append("at.ac.tuwien.dsg.hadl.schema.core:at.ac.tuwien.dsg.hadl.schema.runtime");
            for (String e : extensions) {
                sb.append(":");
                sb.append(e);
            }
			JAXBContext jc = JAXBContext.newInstance(sb.toString());
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            File p = new File(osSpecificPath(path));
            p.mkdirs();
			m.marshal(of.createRuntimeStructure(structure), new File(path + fileName));
			return Option.none();
//		} catch (JAXBException e) {
		} catch (Throwable t) {
			return Option.some(t);
		}
	}

    public Option<Throwable> storeExecutable(HADLmodel model, String path, String fileName) {
		try {			
			JAXBContext jc = JAXBContext.newInstance("at.ac.tuwien.dsg.hadl.schema.core:at.ac.tuwien.dsg.hadl.schema.executable");
            Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            File p = new File(osSpecificPath(path));
            p.mkdirs();
			m.marshal(model, new File(path + fileName));
			return Option.none();
//		} catch (JAXBException e) {
		} catch (Throwable t) {
			return Option.some(t);
		}
	}
	
	public Option<TActivityScope> scope(HADLmodel model, String id) {
		if (id == null) {
			return Option.some(allContainingScope(model));
		}
		for (TStructure struct : model.getHADLstructure()) {
			for (TActivityScope actS : struct.getActivityScope()) {
				if (actS.getId().equals(id)) {
					return Option.some(actS);
				}
			}
		}
		return Option.some(allContainingScope(model));
	}
	
	private TActivityScope allContainingScope(HADLmodel model) {
		TActivityScope scope = new TActivityScope();
		for (TStructure s : model.getHADLstructure()) {
			scope.getCollaboratorRef().addAll(s.getComponent());
			scope.getCollaboratorRef().addAll(s.getConnector());
			scope.getObjectRef().addAll(s.getObject());
			scope.getCollaboratorRelationRef().addAll(s.getCollabRef());
			scope.getObjectRelationRef().addAll(s.getObjectRef());
			scope.getLinkRef().addAll(s.getLink());
		}
		return scope;
	}

	// takes a path in UNIX form and returns the correct form for the host os
	private String osSpecificPath(String path) {
		if ("win".equals(os)) {
			return path.replace('/', '\\');
		}
		return path;
	}
	
}