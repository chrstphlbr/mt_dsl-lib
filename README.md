# hADL Synchronous Library

This repository defines a Java library for calling the [hADL](https://bitbucket.org/christophdorn/hadl) LinkageConnector and RuntimeMonitor in a synchronous fashion. This library is heavily used by Christoph Laaber's [DSL](https://bitbucket.org/chrstphlbr/mt_dsl) from his [master thesis](https://bitbucket.org/chrstphlbr/mt_thesis), but can also be used on its own from Java.

## Required Projects
The only requirement which is not available through a public Maven repository is [hADL](https://bitbucket.org/christophdorn/hadl).
## Install library
Run the following command with Maven 3 from the base directory:
```
mvn clean install
```